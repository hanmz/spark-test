package com.nonequal.service;

import com.clearspring.analytics.util.Lists;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by hanmz on 16-3-18.
 * <p/>
 * zipfan
 */
public class Transition {
  private static final Pattern SPACE = Pattern.compile("\\t");
  private static final File newFile = new File("/home/hanmz/Develop/data/test/new-skew");

  public static void main(String[] args) throws IOException {
    Runtime imp = Runtime.getRuntime();
    System.out.println(imp.maxMemory() / 1024 / 1024);
    System.out.println(imp.freeMemory() / 1024 / 1024);
    List<String> list = Lists.newArrayList();
    //      Files.readLines(new File("/home/hanmz/Develop/data/test/skew"), Charsets.UTF_8);
    LineIterator it =
      FileUtils.lineIterator(new File("/home/hanmz/Develop/data/test/skew"), "UTF-8");
    try {
      while (it.hasNext()) {
        String line = it.nextLine();
        list.add(line);
      }
    } finally {
      LineIterator.closeQuietly(it);
    }
    System.out.println(imp.maxMemory() / 1024 / 1024 + "M");
    System.out.println(imp.freeMemory() / 1024 / 1024 + "M");
    List<Skew> skews = new ArrayList<>();
    int j = 1;
    for (String line : list) {
      j++;
      List<String> s = Arrays.asList(SPACE.split(line));
      Skew l = new Skew(Double.parseDouble(s.get(0)), s.get(1));
      skews.add(l);
      if (j % 1000000 == 0) {
        System.out.println(imp.freeMemory() / 1024 / 1024 + "M");
      }
    }
    Collections.sort(skews, new Comparator<Skew>() {
      public int compare(Skew arg0, Skew arg1) {
        return compareTo(arg0.getKey(), arg1.getKey());
      }
    });

    int num = skews.size();
    int sample = num / 10;
    StringBuilder tmp = new StringBuilder();
    for (int i = 0; i <= sample; i++) {
      tmp.append(skews.get(i).toString());
      //      if (i != 0 && i % 10000 == 0) {
      //        Files.append(tmp.toString(), newFile, Charsets.UTF_8);
      //      }
    }
    Files.append(tmp.toString(), newFile, Charsets.UTF_8);
  }

  private static int compareTo(double a, double b) {
    if (a > b) {
      return 1;
    } else if (a < b) {
      return -1;
    } else {
      return 0;
    }
  }
}


class Skew {
  private double key;
  private String value;

  public Skew(double key, String value) {
    this.key = key;
    this.value = value;
  }

  public double getKey() {
    return key;
  }

  public void setKey(double key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return getKey() + "\t" + getValue() + "\n";
  }
}
