package com.nonequal.tmp;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by hanmz on 16-3-3.
 * <p/>
 * 测试spark 自带的Range Partition算法
 */
public class RangePartitionTest {
  private static final Pattern SPACE = Pattern.compile("\\t");


  public static void main(String[] args) {
    SparkConf sparkConf =
      new SparkConf().setAppName("RangePartition-Test").setMaster("spark://localhost:7077");
    JavaSparkContext sc = new JavaSparkContext(sparkConf);

    int partitionNum = 1024;
    /**
     * zipfan partition分布情况
     */
    //    JavaRDD<String> zipfan = sc.textFile("file:///home/hanmz/Develop/data/test/zipfan", 4);
    //    JavaPairRDD<Integer, String> zipfans =
    //      zipfan.mapToPair(new PairFunction<String, Integer, String>() {
    //        @Override
    //        public Tuple2<Integer, String> call(String s) {
    //          return new Tuple2<>(Integer.parseInt(Arrays.asList(SPACE.split(s)).get(0)), s);
    //        }
    //      }).sortByKey(true, 10);
    //
    //    zipfans.partitions().size();
    //
    //    zipfans.foreachPartition(new VoidFunction<Iterator<Tuple2<Integer, String>>>() {
    //      @Override
    //      public void call(Iterator<Tuple2<Integer, String>> tuple2Iterator) throws Exception {
    //        ArrayList<Integer> list = Lists.newArrayList();
    //        while (tuple2Iterator.hasNext()) {
    //          list.add(tuple2Iterator.next()._1());
    //        }
    //        System.out.println(list.get(0) + " ~ " + list.get(list.size() - 1) + " : " + list.size());
    //      }
    //    });

    /**
     * skew partition分布情况
     */
    JavaRDD<String> skew = sc.textFile("hdfs://localhost:9000/test/skew", 4);
    JavaPairRDD<Double, String> skewss = skew.mapToPair(new PairFunction<String, Double, String>() {
      @Override
      public Tuple2<Double, String> call(String s) {
        return new Tuple2<>(Double.parseDouble(Arrays.asList(SPACE.split(s)).get(0)), s);
      }
    });

    JavaPairRDD<Double, String> skews = skewss.sortByKey(true, partitionNum);

    List<Integer> result =
      skews.mapPartitions(new FlatMapFunction<Iterator<Tuple2<Double, String>>, Integer>() {
        @Override
        public Iterable<Integer> call(Iterator<Tuple2<Double, String>> tuple2Iterator) throws Exception {
          List<Integer> res = Lists.newArrayList();
          ArrayList<Double> list = Lists.newArrayList();
          while (tuple2Iterator.hasNext()) {
            list.add(tuple2Iterator.next()._1());
          }
//          System.out.println(list.get(0) + " ~ " + list.get(list.size() - 1) + " : " + list.size());
          res.add(list.size());
          return res;
        }
      }).collect();

    System.out.println(result.size());
    double max = 0d;
    double sum = 0d;
    for (int i : result) {
      if (max < i) {
        max = i;
      }
      sum += i;
    }
    double avg = sum / partitionNum;
    System.out.println("max = " + max + " avg = " + avg);
    System.out.println("result = " + (max - avg) / avg);
  }

  //    skews.foreachPartition(new VoidFunction<Iterator<Tuple2<Double, String>>>() {
  //      @Override
  //      public void call(Iterator<Tuple2<Double, String>> tuple2Iterator) throws Exception {
  //        ArrayList<Double> list = Lists.newArrayList();
  //        while (tuple2Iterator.hasNext()) {
  //          list.add(tuple2Iterator.next()._1());
  //        }
  //        System.out.println(list.get(0) + " ~ " + list.get(list.size() - 1) + " : " + list.size());
  //      }
  //    });
}
