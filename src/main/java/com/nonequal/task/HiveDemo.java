package com.nonequal.task;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.hive.HiveContext;

import java.util.List;

/**
 * Created by hanmz on 16-2-16.
 * <p>
 * * 注意：
 * 使用HiveContext时
 * 1:需要在classpath下面增加三个配置文件：hive-site.xml,core-site.xml,hdfs-site.xml
 * (当$SPARK_HOME/conf下有hive-site.xml时，可以不设置。core-site.xml和hdfs-site.xml暂时没有看出必要性)
 * 2:需要增加postgresql或mysql驱动包的依赖
 * (设置spark-env.sh 添加 export SPARK_CLASSPATH=$SPARK_CLASSPATH:/usr/local/hive/lib/mysql-connector-java-5.1.38-bin.jar）
 * 3:需要增加hive-jdbc,hive-exec的依赖
 */
public class HiveDemo {
  public static void main(String[] args) {
    SparkConf conf = new SparkConf().setAppName("HiveDemoYarn");
    SparkContext sc = new SparkContext(conf);
    HiveContext hiveCtx = new HiveContext(sc);
    testHive(hiveCtx);
    sc.stop();
  }

  //测试spark sql查询hive上面的表
  public static void testHive(HiveContext hiveCtx) {

    List<Row> result = hiveCtx.sql("SELECT * FROM employee").collectAsList();
    for (Row row : result) {
      System.out.println(
        "i success:" + row.getInt(0) + "," + row.getString(1) + "," + row.getString(2));
    }
  }
}
