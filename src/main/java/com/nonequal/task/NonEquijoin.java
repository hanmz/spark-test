package com.nonequal.task;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.hive.HiveContext;

import java.util.List;

/**
 * Created by hanmz on 16-2-18.
 */
public class NonEquijoin {
  public static void main(String[] args) {
    SparkConf conf = new SparkConf().setAppName("NonEquijoin");
    SparkContext sc = new SparkContext(conf);
    HiveContext hiveCtx = new HiveContext(sc);
    join(hiveCtx);
    sc.stop();
  }

  public static void join(HiveContext hiveCtx) {

    String sql =
      "SELECT /*+ MAPJOIN(o) */ o.*, l.* FROM orders o JOIN (SELECT * FROM lineitem) l WHERE (o.o_totalprice - l.l_extendedprice < 10 or o.o_totalprice - l.l_extendedprice > -10)";
    List<Row> result = hiveCtx.sql(sql).collectAsList();
    for (Row row : result) {
      System.out.println("i success:" + row.getInt(0));
    }
  }
}
