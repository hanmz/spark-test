package com.nonequal.task;

import com.nonequal.bean.AccountBean;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanmz on 16-2-16.
 * <p>
 * 注意：
 * 使用HiveContext时
 * 1:需要在classpath下面增加三个配置文件：hive-site.xml,core-site.xml,hdfs-site.xml
 * 2:需要增加postgresql或mysql驱动包的依赖
 * 3:需要增加hive-jdbc,hive-exec的依赖
 */
public class SimpleDemo {
  public static void main(String[] args) {

  }

  //测试spark sql查询hive上面的表
  public static void testHive(HiveContext hiveCtx) {
    List<Row> result = hiveCtx.sql("SELECT foo,bar,name from pokes2 limit 10").collectAsList();
    for (Row row : result) {
      System.out.println(row.getString(0) + "," + row.getString(1) + "," + row.getString(2));
    }
  }

  //测试spark sql直接查询JSON格式的数据
  public static void testQueryJson(SQLContext sqlCtx) {
    DataFrame rdd = sqlCtx.table("file:///D:/tmp/tmp/json.txt");
    rdd.printSchema();

    // Register the input schema RDD
    rdd.registerTempTable("account");

    DataFrame accs = sqlCtx.sql("SELECT address, email,id,name FROM account ORDER BY id LIMIT 10");
    List<Row> result = accs.collectAsList();
    for (Row row : result) {
      System.out.println(
        row.getString(0) + "," + row.getString(1) + "," + row.getInt(2) + "," + row.getString(3));
    }

    //    JavaRDD<String> names = accs.map(new Function<Row, String>() {
    //      @Override
    //      public String call(Row row) throws Exception {
    //        return row.getString(3);
    //      }
    //    });
    //    System.out.println(names.collect());
  }


  //测试spark sql的自定义函数
  public static void testUDF(JavaSparkContext sc, SQLContext sqlCtx) {
    // Create a account and turn it into a Schema RDD
    ArrayList<AccountBean> accList = new ArrayList<AccountBean>();
    accList.add(new AccountBean(1, "lily", "lily@163.com", "gz tianhe"));
    JavaRDD<AccountBean> accRDD = sc.parallelize(accList);

    DataFrame rdd = sqlCtx.createDataFrame(accRDD, AccountBean.class);

    rdd.registerTempTable("acc");

    //    // 编写自定义函数UDF
    //    sqlCtx.registerFunction("strlength", new UDF1<String, Integer>() {
    //      @Override
    //      public Integer call(String str) throws Exception {
    //        return str.length();
    //      }
    //    }, DataType.IntegerType);
    //
    //    // 数据查询
    //    List<Row> result =
    //      sqlCtx.sql("SELECT strlength('name'),name,address FROM acc LIMIT 10").collect();
    //    for (Row row : result) {
    //      System.out.println(row.getInt(0) + "," + row.getString(1) + "," + row.getString(2));
    //    }
  }
}
