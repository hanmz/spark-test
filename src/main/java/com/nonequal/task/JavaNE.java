package com.nonequal.task;

import com.google.common.collect.Lists;
import org.apache.spark.Partitioner;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;
import scala.Tuple3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.regex.Pattern;

/**
 * Created by hanmz on 16-2-21.
 */

public final class JavaNE {
  private static final Logger LOG = LoggerFactory.getLogger(NonEquijoin.class);
  private static final Pattern SPACE = Pattern.compile("\\|");
  static final double[] rangeBounds =
    {10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000};

  public static void main(String[] args) throws Exception {
    SparkConf sparkConf =
      new SparkConf().setAppName("Spark-Non-Equal").setMaster("spark://localhost:7077");
    JavaSparkContext sc = new JavaSparkContext(sparkConf);

    JavaRDD<String> lineitem = sc.textFile("hdfs://localhost:9000/user/data/lineitem.tbl", 1);
    JavaRDD<String> order = sc.textFile("hdfs://localhost:9000/user/data/orders.tbl", 1);

    /**
     * 转换成 key - value 形式
     */
    JavaPairRDD<Double, Tuple3<String, Double, String>> lineitems =
      lineitem.mapToPair(new PairFunction<String, Double, Tuple3<String, Double, String>>() {
        @Override
        public Tuple2<Double, Tuple3<String, Double, String>> call(String s) {
          double key = Double.parseDouble(Arrays.asList(SPACE.split(s)).get(5));
          return new Tuple2<>(key, new Tuple3<>("l", key, s));
        }
      }).repartitionAndSortWithinPartitions(new CP(10));

    /**
     * 根据 rangeBounds 生成冗余元素,与其余o表元素共同参与partition
     */
    JavaPairRDD<Double, Tuple3<String, Double, String>> orders =
      order.flatMapToPair(new PairFlatMapFunction<String, Double, Tuple3<String, Double, String>>() {
        @Override
        public Iterable<Tuple2<Double, Tuple3<String, Double, String>>> call(String s) {
          double key = Double.parseDouble(Arrays.asList(SPACE.split(s)).get(3));
          ArrayList<Tuple2<Double, Tuple3<String, Double, String>>> tmp = Lists.newArrayList();
          // 将超过100000的直接过滤掉
          if (key > 100000) {
            return tmp;
          }
          tmp.add(new Tuple2<>(key, new Tuple3<>("o", key, s)));
          for (double i : rangeBounds) {
            if (key < i && key - i > -10) {
              tmp.add(new Tuple2<>(i, new Tuple3<>("o", key, s)));
            }
            if (key > i && key - i < 10) {
              tmp.add(new Tuple2<>(i - 0.01, new Tuple3<>("o", key, s)));
            }
          }
          return tmp;
        }
      }).repartitionAndSortWithinPartitions(new CP(10));

    /**
     * union 两个rdd ,目的是可以在统一分区中操作两个rdd中的数据(根据标志'l','o'区分)
     */
    lineitems = sc.union(lineitems, orders);
    //    int[] pi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    //    List<Tuple2<Double, Tuple3<String, Double, String>>>[] lp = lineitems.collectPartitions(pi);
    //    for (Tuple2<Double, Tuple3<String, Double, String>> t : lp[2]) {
    //      System.out.println(t._1() + " " + t._2()._1() + " " + t._2()._2() + " " + t._2()._3());
    //    }
    lineitems.foreachPartition(new CustomMergeFunction());

    sc.stop();
  }

  /**
   * 自定义merge方法
   */
  static class CustomMergeFunction implements VoidFunction<Iterator<Tuple2<Double, Tuple3<String, Double, String>>>> {
    @Override
    public void call(Iterator<Tuple2<Double, Tuple3<String, Double, String>>> iterator) throws Exception {
      ArrayList<Tuple3<Double, String, Integer>> o = Lists.newArrayList();
      ArrayList<Tuple3<Double, String, Integer>> l = Lists.newArrayList();

      // 分组,o l两个列表
      int oSize = 0;
      int lSize = 0;
      while (iterator.hasNext()) {
        Tuple2<Double, Tuple3<String, Double, String>> tmp = iterator.next();
        if ("o".equals(tmp._2()._1())) {
          oSize++;
          int last = o.size() - 1;
          if (last >= 0 && o.get(last)._1().equals(tmp._2()._2())) {
            o.set(last, new Tuple3<>(o.get(last)._1(), tmp._2()._3(), o.get(last)._3() + 1));
          } else {
            o.add(new Tuple3<>(tmp._2()._2(), tmp._2()._3(), 1));
          }
        } else {
          lSize++;
          int last = l.size() - 1;
          if (last >= 0 && l.get(last)._1().equals(tmp._2()._2())) {
            l.set(last, new Tuple3<>(l.get(last)._1(), tmp._2()._3(), l.get(last)._3() + 1));
          } else {
            l.add(new Tuple3<>(tmp._2()._2(), tmp._2()._3(), 1));
          }
        }
      }

      System.out.println(
        l.get(0)._1() + "~" + l.get(l.size() - 1)._1() + " " + "o表的长度为：" + oSize + " " + "l表的长度为："
          + lSize);

      // 统计每个partition中的非等值连接数
      int count = 0;
      for (Tuple3<Double, String, Integer> t1 : l) {
        for (int i = 0; i < o.size(); i++) {
          if (o.get(i)._1() - t1._1() > 10) {
            break;
          } else if (o.get(i)._1() - t1._1() < -10) {
            o.remove(i);
            i--;
          } else {
            count += t1._3() * o.get(i)._3();
          }
        }
      }
      System.out.println(l.get(0)._1() + "~" + l.get(l.size() - 1)._1() + ":" + count);
    }
  }


  /**
   * 按数值宽度等分成10个partition
   */
  static class CP extends Partitioner {
    private int numPartitions;

    public CP(int numPartitions) {
      this.numPartitions = numPartitions;
    }

    public int numPartitions() {
      return numPartitions;
    }

    public int getPartition(Object i) {
      Double tmp = (Double) i;
      if (tmp < 10000) {
        return 0;
      } else if (tmp < 20000) {
        return 1;
      } else if (tmp < 30000) {
        return 2;
      } else if (tmp < 40000) {
        return 3;
      } else if (tmp < 50000) {
        return 4;
      } else if (tmp < 60000) {
        return 5;
      } else if (tmp < 70000) {
        return 6;
      } else if (tmp < 80000) {
        return 7;
      } else if (tmp < 90000) {
        return 8;
      } else {
        return 9;
      }
    }

    /**
     * 重写equal,可以使 union(rdd1, rdd2) 时按分区进行联合
     */
    public boolean equals(Object i) {
      return (i instanceof Partitioner && ((Partitioner) i).numPartitions() == this.numPartitions);
    }
  }
}


