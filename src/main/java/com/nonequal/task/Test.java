package com.nonequal.task;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by hanmz on 16-2-22.
 */
public class Test {
  private static final Logger LOG = LoggerFactory.getLogger(Test.class);
  private static final Pattern SPACE = Pattern.compile("\\|");

  public static void main(String[] args) {
    SparkConf sparkConf = new SparkConf().setAppName("Spark-Non-Equal").setMaster("local");
    JavaSparkContext sc = new JavaSparkContext(sparkConf);

    JavaRDD<String> lineitem = sc.textFile("hdfs://localhost:9000/user/data/lineitem.tbl", 1);
    JavaRDD<String> order = sc.textFile("hdfs://localhost:9000/user/data/orders.tbl", 1);

    JavaPairRDD<Double, String> lineitems =
      lineitem.mapToPair(new PairFunction<String, Double, String>() {
        @Override
        public Tuple2<Double, String> call(String s) {
          double key = Double.parseDouble(Arrays.asList(SPACE.split(s)).get(5));
          return new Tuple2<>(key, s);
        }
      }).sortByKey(true, 1);
    JavaPairRDD<Double, String> orders =
      order.mapToPair(new PairFunction<String, Double, String>() {
        @Override
        public Tuple2<Double, String> call(String s) {
          double key = Double.parseDouble(Arrays.asList(SPACE.split(s)).get(3));
          return new Tuple2<>(key, s);
        }
      }).sortByKey(true, 1);

    List<Tuple2<Double, String>> l = lineitems.collect();
    List<Tuple2<Double, String>> o = orders.collect();
    System.out.println("lineitem最小值：" + l.get(0)._1());
    System.out.println("lineitem最大值：" + l.get(l.size() - 1)._1());
    System.out.println("orders最小值：" + o.get(0)._1());
    System.out.println("orders最大值：" + o.get(o.size() - 1)._1());

    System.out.println("l表分布情况");
    distribution(l, 10000);
    System.out.println("o表分布情况");
    distribution(o, 10000);

  }

  private static void distribution(List<Tuple2<Double, String>> l, int span) {
    int count = 0;
    int num = 1;
    for (int i = 0; i < l.size() - 1; i++) {
      if (l.get(i)._1() <= span * num) {
        count++;
      } else {
        System.out.println(span * num - span + " ~ " + span * num + " : " + count);
        num++;
        count = 0;
      }
    }
    System.out.println(span * num - span + " ~ " + span * num + " : " + count);
  }
}
