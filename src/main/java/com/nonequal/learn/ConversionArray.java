package com.nonequal.learn;

/**
 * Created by hanmz on 16-3-23.
 *
 * 将具有n个元素的向量向左旋转i个位置,不需要额外变量,且只需要遍历一遍元素的算法
 */
public class ConversionArray {
  public static void main(String[] args) {
    int distance = 2;
    int[] arr = {1, 2, 3, 4, 5};
    // 位置交换算法
    int t = arr[0];
    for (int i = 0; i < distance; i++) {
      int n = 0;
      while (i + (n + 1) * distance < arr.length) {
        arr[i + n * distance] = arr[i + ++n * distance];
      }
      if (i < distance - 1) {
        arr[i + n * distance] = arr[i + 1];
      }
      if(i == distance - 1){
        arr[i + n * distance] = t;
      }
    }
    for(int i : arr){
      System.out.println(i);
    }
  }
}
