package com.nonequal.learn;

/**
 * Created by hanmz on 16-3-23.
 * <p/>
 * 变位词
 */
public class SignatureClassify {
  public static void main(String[] args) {
    String[] arr = {"stop", "tops", "main", "name", "mina"};
    for (String str : arr) {
      char[] chars = str.toCharArray();
      sort(chars, 0, chars.length - 1);
      StringBuilder sb = new StringBuilder();
      for (Character c : chars) {
        sb.append(c);
      }
      System.out.println(sb.toString());
    }
  }

  private static void sort(char[] chars, int lefts, int rights) {
    if (lefts < rights) {
      char tmp = chars[lefts];
      int left = lefts;
      int right = rights;
      while (left < right) {
        while (chars[right] >= tmp && right > left) {
          right--;
        }
        chars[left] = chars[right];
        while (chars[left] < tmp && left < right) {
          left++;
        }
        chars[right] = chars[left];
      }
      chars[left] = tmp;
      sort(chars, lefts, left - 1);
      sort(chars, left + 1, rights);
    }
  }
}
