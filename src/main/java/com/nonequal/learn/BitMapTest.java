package com.nonequal.learn;

import java.util.Random;

/**
 * Created by hanmz on 16-3-23.
 */
public class BitMapTest {
  public static void main(String[] args) {
    java.util.BitSet bitSet = new java.util.BitSet(10000);

    int[] origin = new int[10000000];
    // 生成原始数据
    for (int i = 0; i < 10000000; i++) {
      origin[i] = i + 1;
    }
    // 抽样原始数据
    int[] arr = reservoir(origin, 1000000);
    // 位图排序
    for (int i : arr) {
      bitSet.set(i - 1, true);
    }
    // 打印结果
    for (int i = 0; i < bitSet.length(); i++) {
      if (bitSet.get(i)) {
        System.out.println(i + 1);
      }
    }
  }

  // 蓄水池采样算法(均匀随机采样)
  private static int[] reservoir(int[] origin, int k) {
    Random random = new Random();
    int length = origin.length;
    int[] arr = new int[k];
    int i = 0;
    if (length >= k) {
      for (; i < k; i++) {
        arr[i] = origin[i];
      }
      for (; i < length; i++) {
        int index = random.nextInt(i);
        if (index < k) {
          arr[index] = origin[i];
        }
      }
    } else {
      for (; i < length; i++) {
        arr[i] = origin[i];
      }
    }
    return arr;
  }
}
