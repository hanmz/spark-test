package com.nonequal.bean;

/**
 * Created by hanmz on 16-2-16.
 */
public class AccountBean {
  private int id;
  private String name;
  private String email;
  private String desc;

  public AccountBean(int id, String name, String email, String desc) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.desc = desc;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }
}
