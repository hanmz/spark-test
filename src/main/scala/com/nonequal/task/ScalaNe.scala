package com.nonequal.task

import com.nonequal.service.{CustomSampling, CustomPartitioner, DetermineBoundsAlgorithm}
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-3-6.
  */
object ScalaNe {
  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("Scala-Non-Equal").setMaster("spark://localhost:7077")
    val sc = new SparkContext(sparkConf)

    val lineitem = sc.textFile("hdfs://localhost:9000/user/data/lineitem.tbl", 1)
    val order = sc.textFile("hdfs://localhost:9000/user/data/orders.tbl", 1)

    /**
      * 转换成 key - value 形式并分区
      */
    val lineitems = lineitem.map { s =>
      val key = s.split("\\|")(5).toDouble
      (key, ('l', key, s))
    }
    val lMax = lineitems.max._1 // 针对 l o 两表的操作,不通用

    val rangeBounds = new CustomSampling[Double, (Char, Double, String)](10, lineitems).obtainBounds()
    //    val rangeBounds = new DetermineBoundsAlgorithm(lineitem, order).determineBounds()
    val lineitemsPartition = lineitems.repartitionAndSortWithinPartitions(new CustomPartitioner[Double, (Char, Double, String)](rangeBounds))

    val orders = order.flatMap { s =>
      val key = s.split("\\|")(3).toDouble
      val list = ArrayBuffer((key, ('o', key, s)))
      rangeBounds.foreach { a =>
        if (key < a && key - a > -10) {
          list += ((a, ('o', key, s)))
        } else if (key > a && key - a < 10) {
          list += ((a - 0.01, ('o', key, s)))
        }
      }
      list
    }.filter(_._1 < lMax + 10).repartitionAndSortWithinPartitions(new CustomPartitioner[Double, (Char, Double, String)](rangeBounds))

    /**
      * union 两个rdd ,目的是可以在统一分区中操作两个rdd中的数据(根据标志'l','o'区分)
      * 注意：
      * union 时会根据两个partitioner是否相同决定是否将两表相对应的分区划分到一起
      */
    sc.union(lineitemsPartition, orders).foreachPartition { s =>
      val l: ArrayBuffer[(Double, String, Int)] = ArrayBuffer()
      val o: ArrayBuffer[(Double, String, Int)] = ArrayBuffer()

      // 分组,o l两个列表
      var oSize: Int = 0
      var lSize: Int = 0
      var count: Int = 0
      while (s.hasNext) {
        val value = s.next()
        if ('o'.equals(value._2._1)) {
          oSize += 1
          val last = o.size - 1
          if (last >= 0 && o(last)._1 == value._2._2) {
            o(last) = (o(last)._1, value._2._3, o(last)._3 + 1)
          } else {
            o += ((value._2._2, value._2._3, 1))
          }
        } else {
          lSize += 1
          val last = l.size - 1
          if (last >= 0 && l(last)._1 == value._2._2) {
            l(last) = (l(last)._1, value._2._3, l(last)._3 + 1)
          } else {
            l += ((value._2._2, value._2._3, 1))
          }
        }
      }
      println(l(0)._1 + "~" + l(l.size - 1)._1 + " " + "o表的长度为：" + oSize + " " + "l表的长度为：" + lSize)
      //      println("o表的长度为：" + oSize + " " + "l表的长度为：" + lSize)

      //统计每个partition中的非等值连接数
      for (t1 <- l) {
        var t2 = 0
        while (t2 < o.size && o(t2)._1 - t1._1 <= 10) {
          if (o(t2)._1 - t1._1 < -10) {
            o.remove(t2) // 调用remove方法之后size会减1
          } else {
            count += o(t2)._3 * t1._3
            t2 += 1
          }
        }
      }
      println(l(0)._1 + "~" + l(l.size - 1)._1 + ":" + count)
      //      println(count)
    }
    sc.stop()
  }
}
