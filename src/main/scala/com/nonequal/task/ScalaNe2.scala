package com.nonequal.task

import com.nonequal.service._
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-3-19.
  */
object ScalaNe2 {
  // the predicate of query statement |L.a - S.a| <= range
  val range = 0.001
  // the size of partition
  val partitions = 10
  // 0表示蓄水池采样,1表示决策是二维直方图
  var algorithm = 1
  // 决策树二维直方图算法下
  // 0代表面积算法,1代表密度算法
  var schema = 1
  // the scale of samples
  val scale = 1000
  val part = 1


  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("ScalaNe2").setMaster("spark://localhost:7077")
    val sc = new SparkContext(sparkConf)

    val lineitem = sc.textFile("hdfs://localhost:9000/user/data/skew1", 1)
    val order = sc.textFile("hdfs://localhost:9000/user/data/skew2", 1)

    /**
      * 转换成 key - value 形式并分区
      */
    val lineitems = lineitem.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('l', key, s))
    }

    //        自带RangePartition采样定界算法(单表采样)
    //        val rangeBounds = new CustomSampling[Double, (Char, Double, String)](10, lineitems).obtainBounds()
    //        蓄水池(均匀随机)采样定界(方法一)
    //        val rangeBounds = new DetermineBoundsTest().determineBounds(lineitem, order)
    //        决策树采样定界 (存在缺陷)
    //        val rangeBounds = new DecisionTreeDetermineBounds().determineBounds(lineitem, order)
    //        决策树采样二维直方图定界
    //        val rangeBounds = new TwoDimensionHistogram().determineBounds(lineitem, order)
    //        二维直方图面积(方法二)
    //        val rangeBounds = DTSampling.obtainBounds(lineitem.map { s => s.split("\\t")(0).toDouble }, order.map { s => s.split("\\t")(0).toDouble }, 1)
    var rangeBounds: Array[Double] = Array.empty[Double]
    algorithm match {
      case 0 => rangeBounds = new DetermineBoundsTest().determineBounds(lineitem, order)
      case 1 => rangeBounds = DTSampling.obtainBounds(lineitem.map { s => s.split("\\t")(0).toDouble }, order.map { s => s.split("\\t")(0).toDouble }, schema)
      case _ => println("schema algorithm")
    }

    var i = 0
    println("range bound:")
    rangeBounds.foreach { s =>
      i += 1
      println(i + " " + s)
    }

    val lineitemsPartition = lineitems.repartitionAndSortWithinPartitions(new CustomPartitioner[Double, (Char, Double, String)](rangeBounds))
    val orders = order.flatMap { s =>
      val key = s.split("\\t")(0).toDouble
      val list = ArrayBuffer((key, ('o', key, s)))
      list += ((key, ('o', key, s)))
      rangeBounds.foreach { a =>
        if (key < a && key - a > -range) {
          list += ((a + 0.00001d, ('o', key, s)))
        } else if (key > a && key - a < range) {
          list += ((a, ('o', key, s)))
        }
      }
      list
    }.repartitionAndSortWithinPartitions(new CustomPartitioner[Double, (Char, Double, String)](rangeBounds))

    /**
      * union 两个rdd ,目的是可以在统一分区中操作两个rdd中的数据(根据标志'l','o'区分)
      * 注意：
      * union 时会根据两个partitioner是否相同决定是否将两表相对应的分区划分到一起
      */
    sc.union(lineitemsPartition, orders).foreachPartition { s =>
      val l: ArrayBuffer[(Double, String, Int)] = ArrayBuffer()
      val o: ArrayBuffer[(Double, String)] = ArrayBuffer()

      // 分组,o l两个列表
      var lSize: Int = 0
      var last: Int = -1
      while (s.hasNext) {
        val value = s.next()
        if ('o'.equals(value._2._1)) {
          o += ((value._2._2, value._2._3))
        } else {
          // 因为l表是有序的,所以可以直接进行下面的合并操作
          if (last >= 0 && l(last)._1 == value._2._2) {
            l(last) = (l(last)._1, value._2._3, l(last)._3 + 1)
          } else {
            l += ((value._2._2, value._2._3, 1))
            last += 1
          }
          lSize += 1
        }
      }
      o.sortBy(_._1)
      val oSize = o.size

      //统计每个partition中的非等值连接数
      var count: Double = 0d
      for (t1 <- l) {
        var t2 = 0
        while (t2 < o.size && o(t2)._1 - t1._1 <= range) {
          if (o(t2)._1 - t1._1 < -range) {
            o.remove(t2) // 调用remove方法之后o.size会执行减1操作(犯了一个错误,加了t2减1操作,导致数组越界)
          } else {
            count += t1._3
            t2 += 1
          }
        }
      }
      println(l(0)._1 + "~" + l(l.size - 1)._1 + " o表长度：" + oSize + " l表长度：" + lSize + " 连接数：" + count)
    }
    sc.stop()
  }
}
