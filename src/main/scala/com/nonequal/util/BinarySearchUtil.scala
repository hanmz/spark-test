package com.nonequal.util

/**
  * Created by hanmz on 16-3-28.
  */
object BinarySearchUtil {
  val binarySearch: ((Array[Double], Double) => Int) = CollectionsUtils.makeBinarySearch[Double]

  def abs(n1: Double, n2: Double): Double = {
    if (n1 > n2) {
      n1 - n2
    } else {
      n2 - n1
    }
  }
}
