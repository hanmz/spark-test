package com.nonequal.tmp

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-4-7.
  */
class ArrayBufferTest {

}

object ArrayBufferTest {
  def main(args: Array[String]) {
    val l: ArrayBuffer[Int] = ArrayBuffer()
    l += 1
    l += 2
    l += 3
    l += 4
    l += 5
    var i = 0
    while (i < l.size) {
      if (l(i) < 5) {
        l.remove(i)
        println("size:" + l.size)
      } else {
        i += 1
      }
    }
    l.foreach(println)
  }
}
