package com.nonequal.tmp

import com.nonequal.util.BinarySearchUtil

/**
  * Created by hanmz on 16-3-29.
  */
object TestBinarySearch {
  def main(args: Array[String]) {
    val arr = Array[Double](1, 3, 5, 7, 9)
    val position = BinarySearchUtil.binarySearch(arr, 0)
    println(-position - 1)
  }
}
