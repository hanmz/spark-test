package com.nonequal.tmp

import org.apache.spark.{SparkContext, SparkConf}

/**
  * Created by hanmz on 16-3-29.
  */
object TestSkew {
  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("DecisionTreeSampling").setMaster("local")
    val sc = new SparkContext(sparkConf)
    val skew = sc.textFile("file:///home/hanmz/Develop/data/test/skew")

    val skews = skew.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('l', key, s))
    }
    val result = skews.map(s => s._1).filter(s => s > 5.242858803941228 - 1 && s < 5.242858803941228 + 1).count()
    println(result)
  }

}
