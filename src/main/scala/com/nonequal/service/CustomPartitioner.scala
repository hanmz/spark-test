package com.nonequal.service

import com.nonequal.util.CollectionsUtils
import org.apache.spark.Partitioner

import scala.reflect.ClassTag

/**
  * Created by hanmz on 16-3-7.
  *
  * 自定义 partitioner 必须实现Partitioner的两个方法
  * numPartitions
  * getPartition
  *
  */
class CustomPartitioner[K: Ordering : ClassTag, V](bounds: Array[K], private var ascending: Boolean = true) extends Partitioner {

  private val rangeBounds = bounds
  private val ordering = implicitly[Ordering[K]]

  def numPartitions: Int = rangeBounds.length + 1

  private val binarySearch: ((Array[K], K) => Int) = CollectionsUtils.makeBinarySearch[K]

  def getPartition(key: Any): Int = {
    val k = key.asInstanceOf[K]
    var partition = 0
    if (rangeBounds.length <= 128) {
      // If we have less than 128 partitions naive search
      while (partition < rangeBounds.length && ordering.gt(k, rangeBounds(partition))) {
        partition += 1
      }
    } else {
      // Determine which binary search method to use only once.
      partition = binarySearch(rangeBounds, k)
      // binarySearch either returns the match location or -[insertion point]-1
      if (partition < 0) {
        partition = -partition - 1
      }
      if (partition > rangeBounds.length) {
        partition = rangeBounds.length
      }
    }
    if (ascending) {
      partition
    } else {
      rangeBounds.length - partition
    }
  }

  /**
    * 重写 equal 判断两partitioner是否相同(目的: 为了两表 union 服务)
    */
  override def equals(other: Any): Boolean = other match {
    case r: CustomPartitioner[_, _] =>
      r.rangeBounds.sameElements(rangeBounds) && r.ascending == ascending
    case _ =>
      false
  }

  override def hashCode(): Int = {
    val prime = 31
    var result = 1
    var i = 0
    while (i < rangeBounds.length) {
      result = prime * result + rangeBounds(i).hashCode
      i += 1
    }
    result = prime * result + ascending.hashCode
    result
  }
}
