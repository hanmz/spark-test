package com.nonequal.service

import com.nonequal.task.ScalaNe2
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-3-19.
  *
  * 决策数定界算法
  */
object DecisionTreeDetermineBounds {
  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("Partition-Algorithm").setMaster("local")
    val sc = new SparkContext(sparkConf)
    val skew1 = sc.textFile("hdfs://localhost:9000/user/data/skew1", 1)
    val skew2 = sc.textFile("hdfs://localhost:9000/user/data/skew2", 1)
    val list = new DecisionTreeDetermineBounds().determineBounds(skew1, skew2)
    println("result:")
    list.foreach(println)
    //    sc.stop()
  }

  /**
    * 将两表采样数据合并(放入object中为了方便其他对象使用)
    * l o 两表是有序的
    */
  def merge(l: Array[(Double, Double, Char)], o: Array[(Double, Double, Char)]): Array[(Double, Double, Char)] = {
    val buffer = ArrayBuffer.empty[(Double, Double, Char)]
    var i = 0
    var j = 0
    while (i < l.length && j < o.length) {
      if (l(i)._1 <= o(j)._1) {
        buffer += l(i)
        i += 1
      } else {
        buffer += o(j)
        j += 1
      }
    }
    while (i < l.length) {
      buffer += l(i)
      i += 1
    }
    while (j < o.length) {
      buffer += o(j)
      j += 1
    }
    buffer.toArray
  }
}

class DecisionTreeDetermineBounds {
  def determineBounds(lineitem: RDD[String], order: RDD[String]): Array[Double] = {
    /**
      * 第一步 sampling
      */
    // 1.将l o表转成key-value形式
    val lineitems = lineitem.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('l', key, s))
    }
    val orders = order.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('o', key, s))
    } // l的上边界远远小于o的上边界,在此处进行一次过滤

    //  2.分别对l o表采样
    val lSketch = new DecisionTreeSampling(lineitems.map(s => s._1), 'l').obtainSamples()
    // 抽样元素占总元素的比例
    val ratio = lSketch._1.length.toDouble / lSketch._2.toDouble
    // 对主表samples排序
    val lSamples = lSketch._1.sortBy(_._1)
    // 获取 lSamples 中第一列
    val o = new DecisionTreeSampling(orders.map(o => o._1), 'o')
    val oSketch = o.obtainSamples(lSamples.map(s => s._1))
    // 对副表samples排序
    val oSamples = oSketch._1.sortBy(_._1)

    // 获取元素连接信息,连接信息与lSamples中抽样元素相对应
    val avgWeight = oSketch._3
    val joinNum = o.joinNums.map(s => s * avgWeight * ratio)



    println(lSamples.length + " " + oSamples.length)
    var sum = 0d
    lSamples.foreach(s => sum += s._2)
    println("l weight:" + sum)
    sum = 0d
    oSamples.foreach(s => sum += s._2)
    println("o weight:" + sum)
    joinNum.foreach(s => println("join num" + s))

    // 3.采样数据进行连接操作(当数据量增大之后，此处最耗时), 设置两边宽度以及两边拥有副表抽样元素个数
    val lCount = ArrayBuffer[(Double, Int, Int, Double, Double)]()
    var count = 0
    for (t1 <- lSamples) {
      lCount += ((1d, 0, 0, 0, 0))
      for (t2 <- count until oSamples.length if oSamples(t2)._1 - t1._1 <= ScalaNe2.range) {
        if (oSamples(t2)._1 - t1._1 < -ScalaNe2.range) {
          count += 1
        } else {
          if (oSamples(t2)._1 - t1._1 < 0) {
            lCount(lCount.length - 1) = (lCount(lCount.length - 1)._1, lCount(lCount.length - 1)._2 + 1, lCount(lCount.length - 1)._3, lCount(lCount.length - 1)._4 + oSamples(t2)._2, lCount(lCount.length - 1)._5)
          } else if (oSamples(t2)._1 - t1._1 > 0) {
            lCount(lCount.length - 1) = (lCount(lCount.length - 1)._1, lCount(lCount.length - 1)._2, lCount(lCount.length - 1)._3 + 1, lCount(lCount.length - 1)._4, lCount(lCount.length - 1)._5 + oSamples(t2)._2)
          } else {
            lCount(lCount.length - 1) = (lCount(lCount.length - 1)._1, lCount(lCount.length - 1)._2, lCount(lCount.length - 1)._3, lCount(lCount.length - 1)._4, lCount(lCount.length - 1)._5)
          }
        }
      }
    }

    /**
      * 第二步 determine bounds
      *
      * 自定义定界算法
      */
    // 1.合并抽样
    val ordering = implicitly[Ordering[Double]]
    val mergeSample = DecisionTreeDetermineBounds.merge(lSamples, oSamples)
    val numCandidates = mergeSample.length

    // 2.计算总宽度(包括每个元素宽度,连接个数宽度,其中连接个数宽度需斟酌)
    var sumWeights = 0d
    var c = 0
    var i = 0
    mergeSample.foreach { s =>
      if (s._3 == 'l') {
        println(s._2 + " " + joinNum(i) + " " + ratio)
        sumWeights += lCount(c)._1 * s._2 + joinNum(i)
        i += 1
        c += 1
      } else {
        sumWeights += s._2
      }
    }

    // 3.设置初始步长, 加1为了更好的覆盖更多情况
    val step = sumWeights / (ScalaNe2.partitions + 1)

    // 4.执行计算 bounds range
    var flag = true
    var cumWeight = 0.0
    val bounds = ArrayBuffer.empty[Double]
    var previousBound = Option.empty[Double]
    var widths = ArrayBuffer.empty[Double]
    var nextStart = 0 // 下一次循环开始的位置
    var nextAdd = 0d // 下一次增加的步长
    var first = 0 // 0代表第一次循环,1代表不是第一次循环
    var leftCount = 0 // 记录lCount位置

    // 外层循环,大约循环xxx次
    while (flag) {
      var i = 0
      var j = first
      var count = 0
      var target = step + nextAdd // 每次循环tag初始化后都不会改变
      if (first == 1) {
        i = nextStart + 1
        count = leftCount + 1
        widths += target
        bounds += mergeSample(i)._1
        previousBound = Some(mergeSample(i)._1)
      }

      while ((i < numCandidates) && (j < ScalaNe2.partitions - 1)) {
        var tmpLeft = 0d
        var tmpRight = 0d
        val (key, weight, tag) = mergeSample(i)
        if (tag == 'l') {
          tmpLeft = lCount(count)._4
          if (tmpLeft > cumWeight) {
            cumWeight = tmpLeft
          }
          cumWeight += weight * lCount(count)._1 + joinNum(count)
          tmpRight = lCount(count)._5
          count += 1
        } else {
          cumWeight += weight
        }
        if (cumWeight + tmpRight >= target) {
          // Skip duplicate values.
          if (previousBound.isEmpty || ordering.gt(key, previousBound.get)) {
            // 只有第一次循环的时候执行
            if (bounds.isEmpty) {
              nextStart = i
              if (count > 0) {
                leftCount = count - 1
              } else {
                leftCount = 0
              }
            }
            // 将 key 相同的加到一起
            while (i + 1 < numCandidates && key == mergeSample(i + 1)._1) {
              i += 1
              val (weight, tag) = (mergeSample(i)._2, mergeSample(i)._3)
              if (tag == 'l') {
                tmpLeft = lCount(count)._4
                if (tmpLeft > cumWeight) {
                  cumWeight = tmpLeft
                }
                cumWeight += weight * lCount(count)._1 + joinNum(count)
                count += 1
              } else {
                cumWeight += weight
              }
            }
            widths += cumWeight + tmpRight
            bounds += key
            cumWeight = 0
            j += 1
            previousBound = Some(key)
          }
        }
        i += 1
      }
      // 计算最后一部分宽度
      var lastBound = 0d
      while (i < numCandidates) {
        val (weight, tag) = (mergeSample(i)._2, mergeSample(i)._3)
        if (tag == 'l') {
          val tmpLeft = lCount(count)._4
          if (tmpLeft > lastBound) {
            lastBound = tmpLeft
          }
          lastBound += weight * lCount(count)._1 + joinNum(count)
          count += 1
        } else {
          lastBound += weight
        }
        i += 1
      }
      widths += lastBound

      // 第一次循环可能不改变 nextAdd
      if (first == 0) {
        while (mergeSample(nextStart)._1 == mergeSample(nextStart + 1)._1) {
          nextStart += 1
          val (weight, tag) = (mergeSample(nextStart)._2, mergeSample(nextStart)._3)
          if (tag == 'l') {
            leftCount += 1
            nextAdd += weight * lCount(leftCount)._1 + joinNum(leftCount)
          } else {
            nextAdd += weight
          }
        }
      } else {
        var isFirst = true // 标志作用
        var flag1 = true
        while (flag1) {
          nextStart += 1
          val (weight, tag) = (mergeSample(nextStart)._2, mergeSample(nextStart)._3)
          if (tag == 'l') {
            leftCount += 1
            nextAdd += weight * lCount(leftCount)._1 + joinNum(leftCount)
            if (isFirst) {
              nextAdd += lCount(leftCount)._5
            }
            isFirst = false
          } else {
            nextAdd += weight
          }
          if (mergeSample(nextStart)._1 != mergeSample(nextStart + 1)._1) {
            flag1 = false
          }
        }
      }

      first = 1 // 第一次循环为0,以后均为1
      if (bounds.length == ScalaNe2.partitions - 1) {
        computeResult(bounds, widths)
      } else {
        flag = false
      }
    }
    result
  }

  def getVariance(arr: Array[Double]): Double = {
    val average = arr.sum / arr.length
    var num = 0d
    for (i <- arr) {
      num += (i - average) * (i - average)
    }
    num / arr.length
  }

  // bound 方差
  var firstVariance = 0d
  var result = Array.empty[Double]

  def computeResult(bounds: ArrayBuffer[Double], widths: ArrayBuffer[Double]): Array[Double] = {
    val widthsVariance = getVariance(widths.toArray)

    if (result.isEmpty) {
      result = bounds.toArray
      firstVariance = widthsVariance
    } else {
      //      println(firstVariance + " " + widthsVariance)
      if (firstVariance > widthsVariance) {
        firstVariance = widthsVariance
        result = bounds.toArray
      }
    }
    //    widths.foreach(s => print(s + " "))
    //    println()
    //    bounds.foreach(s => print(s + " "))
    //    println()
    bounds.clear()
    widths.clear()

    result
  }

}
