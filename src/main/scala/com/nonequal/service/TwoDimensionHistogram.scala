package com.nonequal.service

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-4-6.
  */
object TwoDimensionHistogram {
  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("DecisionTreeSampling").setMaster("spark://localhost:7077")
    val sc = new SparkContext(sparkConf)
    val skew1 = sc.textFile("hdfs://localhost:9000/user/data/skew2", 1)
    val skew2 = sc.textFile("hdfs://localhost:9000/user/data/skew3", 1)
    new TwoDimensionHistogram().determineBounds(skew1, skew2)
  }

  def determineBounds(mergeSamples: Array[(Double, Double)], partitions: Int): Array[(Double, Double)] = {
    val ordering = implicitly[Ordering[Double]]
    val numCandidates = mergeSamples.length
    var sumWeights = mergeSamples.map(_._2).sum
    val step = sumWeights / partitions
    var cumWeight = 0.0
    val bounds = ArrayBuffer.empty[(Double, Double)]
    var i = 0
    var j = 0
    var previousBound = Option.empty[Double]
    while ((i < numCandidates) && (j < partitions - 1)) {
      val (key, weight) = mergeSamples(i)
      cumWeight += weight
      if (cumWeight >= step) {
        // Skip duplicate values.
        if (previousBound.isEmpty || ordering.gt(key, previousBound.get)) {
          bounds += ((key, cumWeight))
          sumWeights -= cumWeight
          cumWeight = 0.0
          j += 1
          previousBound = Some(key)
        }
      }
      i += 1
    }
    val key = bounds(0)._1 * 2 - bounds(1)._1
    if (key < 0) {
      bounds.insert(0, (0, 0))
    } else {
      bounds.insert(0, (key, 0))
    }
    bounds += ((bounds(partitions - 1)._1 * 2 - bounds(partitions - 2)._1, sumWeights))
    bounds.toArray
  }

}

class TwoDimensionHistogram {
  def determineBounds(lineitem: RDD[String], order: RDD[String]): Unit = {
    /**
      * 第一步 sampling
      */
    // 1.将l o表转成key-value形式
    val lineitems = lineitem.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('l', key, s))
    }
    val orders = order.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('o', key, s))
    }

    //  2.分别对l o表采样(这个地方采用决策树采样,类似于dth.addItem(d)这个过程)
    val lSketch = new DecisionTreeSampling(lineitems.map(s => s._1), 'l').samples()
    val oSketch = new DecisionTreeSampling(orders.map(s => s._1), 'o').samples()

    //  3.对samples排序
    val lSamples = lSketch.sortBy(_._1).map(s => (s._1, s._2))
    val oSamples = oSketch.sortBy(_._1).map(s => (s._1, s._2))


    // 合并两表采样数据
    //    val mergeSample = DecisionTreeDetermineBounds.merge(lSamples, oSamples)
    //    val mergeSamples = mergeSample.map(s => (s._1, s._2))

    println("l Samples size : " + lSamples.length)
    println("o Samples size : " + oSamples.length)
    //    println("merge Sample size : " + mergeSample.length)
    //    println("merge Samples size : " + mergeSamples.length)

    // 4.测试过程中分成
    val ls = TwoDimensionHistogram.determineBounds(lSamples, 7)
    val os = TwoDimensionHistogram.determineBounds(oSamples, 7)

    // 5.获取中点
    val lbuffer = ArrayBuffer.empty[(Double, Double)]
    val obuffer = ArrayBuffer.empty[(Double, Double)]
    var i = 0
    while (i < 7) {
      lbuffer += (((ls(i)._1 + ls(i + 1)._1) / 2, ls(i + 1)._2))
      obuffer += (((os(i)._1 + os(i + 1)._1) / 2, os(i + 1)._2))
      i += 1
    }

    println("l:")
    ls.foreach { s =>
      println(s._1 + " " + s._2)
    }
    println("lbuffer:")
    lbuffer.foreach { s =>
      println(s._1 + " " + s._2)
    }
    println("o:")
    os.foreach { s =>
      println(s._1 + " " + s._2)
    }
    println("obuffer:")
    obuffer.foreach { s =>
      println(s._1 + " " + s._2)
    }
  }
}

