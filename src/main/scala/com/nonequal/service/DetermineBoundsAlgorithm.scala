package com.nonequal.service

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-3-12.
  */
class DetermineBoundsAlgorithm(lineitem: RDD[String], order: RDD[String]) {
  def determineBounds(): Array[Double] = DetermineBoundsAlgorithm.determineBounds(lineitem, order)
}

object DetermineBoundsAlgorithm {
  private val partitions = 10
  private val range = 10

  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("Partition-Algorithm").setMaster("local")
    val sc = new SparkContext(sparkConf)
    val lineitem = sc.textFile("hdfs://localhost:9000/user/data/lineitem.tbl", 1)
    val order = sc.textFile("hdfs://localhost:9000/user/data/orders.tbl", 1)
    val list = determineBounds(lineitem, order)
    list.foreach(println)
    sc.stop()
  }

  def determineBounds(lineitem: RDD[String], order: RDD[String]): Array[Double] = {
    /**
      * 第一步 sampling
      */
    // 1.将l o表转成key-value形式
    val lineitems = lineitem.map {
      s =>
        val key = s.split("\\|")(5).toDouble
        (key, ('l', key, s))
    }
    val lMax = lineitems.max._1
    val orders = order.map {
      s =>
        val key = s.split("\\|")(3).toDouble
        (key, ('o', key, s))
    }.filter(_._1 < lMax + range) // l的上边界远远小于o的上边界,在此处进行一次过滤

    //  2.分别对l o表采样
    val lSketch = new SamplingAlgorithm[Double, (Char, Double, String)](partitions, lineitems, 'l').obtainSamples()
    //    val lNumItem = lSketch._2
    val lSamples = lSketch._1.sortBy(_._1)
    val oSketch = new SamplingAlgorithm[Double, (Char, Double, String)](partitions, orders, 'o').obtainSamples()
    //    val oNumItem = oSketch._2
    val oSamples = oSketch._1.sortBy(_._1)
    //    val oSamples = oSketch._1.sortBy(_._1)

    println(lSamples.length + " " + oSamples.length)
    var sum = 0f
    lSamples.foreach(s => sum += s._2)
    println("l weight:" + sum)
    sum = 0f
    oSamples.foreach(s => sum += s._2)
    println("o weight:" + sum)

    val lCount = ArrayBuffer[(Int, Int, Int, Double, Double)]()

    // 3.采样数据进行连接操作(当数据量增大之后，此处最耗时)
    var count = 0
    for (t1 <- lSamples) {
      lCount += ((1, 0, 0, 0, 0))
      for (t2 <- count until oSamples.length if oSamples(t2)._1 - t1._1 <= range) {
        if (oSamples(t2)._1 - t1._1 < -range) {
          count += 1
        } else {
          if (oSamples(t2)._1 - t1._1 < 0) {
            lCount(lCount.length - 1) = (lCount(lCount.length - 1)._1 + 1, lCount(lCount.length - 1)._2 + 1, lCount(lCount.length - 1)._3, lCount(lCount.length - 1)._4 + oSamples(t2)._2, lCount(lCount.length - 1)._5)
          } else if (oSamples(t2)._1 - t1._1 > 0) {
            lCount(lCount.length - 1) = (lCount(lCount.length - 1)._1 + 1, lCount(lCount.length - 1)._2, lCount(lCount.length - 1)._3 + 1, lCount(lCount.length - 1)._4, lCount(lCount.length - 1)._5 + oSamples(t2)._2)
          } else {
            lCount(lCount.length - 1) = (lCount(lCount.length - 1)._1 + 1, lCount(lCount.length - 1)._2, lCount(lCount.length - 1)._3, lCount(lCount.length - 1)._4, lCount(lCount.length - 1)._5)
          }
        }
      }
    }

    /**
      * 第二步 determine bounds
      *
      * 自定义定界算法
      */
    // 1.合并抽样
    val ordering = implicitly[Ordering[Double]]
    val mergeSample = merge(lSamples, oSamples)
    val numCandidates = mergeSample.length

    // 2.计算总宽度(包括每个元素宽度,连接个数宽度,其中连接个数宽度需斟酌)
    var sumWeights = 0d
    var c = 0
    mergeSample.foreach { s =>
      if (s._3 == 'l') {
        sumWeights += lCount(c)._1 * s._2
        c += 1
      } else {
        sumWeights += s._2
      }
    }

    // 3.设置初始步长, 加1为了更好的覆盖更多情况
    val step = sumWeights / (partitions + 1)

    // 4.执行计算 bounds range
    var flag = true
    var cumWeight = 0.0
    val bounds = ArrayBuffer.empty[Double]
    var previousBound = Option.empty[Double]
    var widths = ArrayBuffer.empty[Double]
    var nextStart = 0 // 下一次循环开始的位置
    var nextAdd = 0d // 下一次增加的步长
    var first = 0 // 0代表第一次循环,1代表不是第一次循环
    var leftCount = 0 // 记录lCount位置

    // 外层循环,大约循环xxx次
    while (flag) {
      var i = 0
      var j = first
      var count = 0
      var target = step + nextAdd // 每次循环tag初始化后都不会改变
      if (first == 1) {
        i = nextStart + 1
        count = leftCount + 1
        widths += target
        bounds += mergeSample(i)._1
        previousBound = Some(mergeSample(i)._1)
      }

      while ((i < numCandidates) && (j < partitions - 1)) {
        var tmpLeft = 0d
        var tmpRight = 0d
        val (key, weight, tag) = mergeSample(i)
        if (tag == 'l') {
          tmpLeft = lCount(count)._4
          if (tmpLeft > cumWeight) {
            cumWeight = tmpLeft
          }
          cumWeight += weight * lCount(count)._1
          tmpRight = lCount(count)._5
          count += 1
        } else {
          cumWeight += weight
        }
        if (cumWeight + tmpRight >= target) {
          // Skip duplicate values.
          if (previousBound.isEmpty || ordering.gt(key, previousBound.get)) {
            // 只有第一次循环的时候执行
            if (bounds.isEmpty) {
              nextStart = i
              if (count > 0) {
                leftCount = count - 1
              } else {
                leftCount = 0
              }
            }
            // 将 key 相同的加到一起
            while (i + 1 < numCandidates && key == mergeSample(i + 1)._1) {
              i += 1
              val (weight, tag) = (mergeSample(i)._2, mergeSample(i)._3)
              if (tag == 'l') {
                tmpLeft = lCount(count)._4
                if (tmpLeft > cumWeight) {
                  cumWeight = tmpLeft
                }
                cumWeight += weight * lCount(count)._1
                count += 1
              } else {
                cumWeight += weight
              }
            }
            widths += cumWeight + tmpRight
            bounds += key
            cumWeight = 0
            j += 1
            previousBound = Some(key)
          }
        }
        i += 1
      }
      // 计算最后一部分宽度
      var lastBound = 0d
      while (i < numCandidates) {
        val (weight, tag) = (mergeSample(i)._2, mergeSample(i)._3)
        if (tag == 'l') {
          val tmpLeft = lCount(count)._4
          if (tmpLeft > lastBound) {
            lastBound = tmpLeft
          }
          lastBound += weight * lCount(count)._1
          count += 1
        } else {
          lastBound += weight
        }
        i += 1
      }
      widths += lastBound

      // 第一次循环可能不改变 nextAdd
      if (first == 0) {
        while (mergeSample(nextStart)._1 == mergeSample(nextStart + 1)._1) {
          nextStart += 1
          val (weight, tag) = (mergeSample(nextStart)._2, mergeSample(nextStart)._3)
          if (tag == 'l') {
            leftCount += 1
            nextAdd += weight * lCount(leftCount)._1
          } else {
            nextAdd += weight
          }
        }
      } else {
        var isFirst = true // 标志作用
        var flag1 = true
        while (flag1) {
          nextStart += 1
          val (weight, tag) = (mergeSample(nextStart)._2, mergeSample(nextStart)._3)
          if (tag == 'l') {
            leftCount += 1
            nextAdd += weight * lCount(leftCount)._1
            if (isFirst) {
              nextAdd += lCount(leftCount)._5
            }
            isFirst = false
          } else {
            nextAdd += weight
          }
          if (mergeSample(nextStart)._1 != mergeSample(nextStart + 1)._1) {
            flag1 = false
          }
        }
      }

      first = 1 // 第一次循环为0,以后均为1
      if (bounds.length == partitions - 1) {
        computeResult(bounds, widths)
      } else {
        flag = false
      }
    }
    result
  }

  def merge(l: Array[(Double, Float, Char)], o: Array[(Double, Float, Char)]): Array[(Double, Float, Char)] = {
    val buffer = ArrayBuffer.empty[(Double, Float, Char)]
    var i = 0
    var j = 0
    while (i < l.length && j < o.length) {
      if (l(i)._1 <= o(j)._1) {
        buffer += l(i)
        i += 1
      } else {
        buffer += o(j)
        j += 1
      }
    }
    while (i < l.length) {
      buffer += l(i)
      i += 1
    }
    while (j < o.length) {
      buffer += o(j)
      j += 1
    }
    buffer.toArray
  }

  def getVariance(arr: Array[Double]): Double = {
    val average = arr.sum / arr.length
    var num = 0d
    for (i <- arr) {
      num += (i - average) * (i - average)
    }
    num / arr.length
  }

  // bound 方差
  var firstVariance = 0d
  var result = Array.empty[Double]

  def computeResult(bounds: ArrayBuffer[Double], widths: ArrayBuffer[Double]): Array[Double] = {
    val widthsVariance = getVariance(widths.toArray)

    if (result.isEmpty) {
      result = bounds.toArray
      firstVariance = widthsVariance
    } else {
      println(firstVariance + " " + widthsVariance)
      if (firstVariance > widthsVariance) {
        firstVariance = widthsVariance
        result = bounds.toArray
      }
    }
    widths.foreach(s => print(s + " "))
    println()
    bounds.foreach(s => print(s + " "))
    println()
    bounds.clear()
    widths.clear()

    result
  }

}
