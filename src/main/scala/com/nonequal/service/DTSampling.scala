package com.nonequal.service

import com.nonequal.task.ScalaNe2
import com.nonequal.util.SamplingUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-4-19.
  */
object DTSampling {
  val range = ScalaNe2.range * 2

  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("DTSampling").setMaster("spark://localhost:7077")
    val sc = new SparkContext(sparkConf)
    val skew = sc.textFile("hdfs://localhost:9000/user/data/skew1", 1)
    val skews = skew.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('l', key, s))
    }

    val result = obtainBounds(skew.map { s => s.split("\\t")(0).toDouble }, skew.map { s => s.split("\\t")(0).toDouble }, 1)
    //    val result = new DTSampling(skew.map { s => s.split("\\t")(0).toDouble }).samples(false)
  }

  def obtainBounds(rdd1: RDD[Double], rdd2: RDD[Double], schema: Int): Array[Double] = {

    // 主表
    val bins1: ArrayBuffer[(Double, Int)] = new DTSampling(rdd1).samples(false)
    var i = 0
    println("bins1:")
    bins1.foreach { s =>
      i += 1
      println(i + " " + s)
    }
    println("bins1.size=" + bins1.size)

    // 副表
    val bins2: ArrayBuffer[(Double, Int)] = new DTSampling(rdd2).samples(false)
    i = 0
    println("bins2:")
    bins2.foreach { s =>
      i += 1
      println(i + " " + s)
    }
    println("bins2.size=" + bins2.size)

    val area = areas(bins1, bins2, schema)
    i = 0
    println("area: ")
    area.foreach { s =>
      i += 1
      println(i + " " + s._1 + " " + s._2)
    }

    val result = determineBounds(area)

    i = 0
    println("schema=" + schema)
    result.foreach { s =>
      i += 1
      println(i + " " + s)
    }

    result
  }

  def areas(bins1: ArrayBuffer[(Double, Int)], bins2: ArrayBuffer[(Double, Int)], schema: Int): ArrayBuffer[(Double, Double)] = {

    val center1 = ArrayBuffer.empty[Double]
    val center2 = ArrayBuffer.empty[Double]

    var i = 0
    while (i < bins1.size - 1) {
      //      center1 += ((bins1(i)._1 , bins1(i)._2 + bins1(i + 1)._2 / 2))
      center1 += (bins1(i)._1 + bins1(i + 1)._1) / 2
      i += 1
    }
    i = 0
    while (i < bins2.size - 1) {
      center2 += (bins2(i)._1 + bins2(i + 1)._1) / 2
      i += 1
    }

    val tmp = ArrayBuffer.empty[Double]
    center1.foreach { s =>
      tmp += s
    }
    center2.foreach { s =>
      tmp += s
    }
    val r = tmp.sorted.toArray

    // 合并center中key相同的
    val center = ArrayBuffer.empty[(Double, Double)]
    if (r.nonEmpty) {
      center += ((r(0), 0))
    }
    i = 0
    var j = 0
    while (i < r.length) {
      if (center(j)._1 != r(i)) {
        center += ((r(i), 0))
        j += 1
      }
      i += 1
    }
    println("center:")
    center.foreach { s =>
      println(s._1 + " " + s._2)
    }
    println("center.size = " + center.size)

    var area = ArrayBuffer.empty[(Double, Double)]
    schema match {
      case 0 =>

        /**
          * 面积
          */
        var i = 0
        while (i < center.size) {
          //        val value = center(i)._1 + ScalaNe2.range
          var value = center(i)._1
          var m = 1
          var x = 0.0
          var y = 0.0
          var flag = true
          while (m < bins1.size && flag) {
            if (value >= bins1(m)._1) {
              if (i > 0 && center(i - 1)._1 > bins1(m - 1)._1) {
                x += (bins1(m)._1 - center(i - 1)._1) / (bins1(m)._1 - bins1(m - 1)._1) * (bins1(m)._2 + bins1(m - 1)._2) / 2
              } else {
                x += (bins1(m)._2 + bins1(m - 1)._2) / 2
              }
              if (i > 0 && center(i - 1)._1 >= bins1(m - 1)._1) {
                bins1.remove(m - 1)
              } else {
                m += 1
              }
            } else {
              if (i > 0 && center(i - 1)._1 > bins1(m - 1)._1) {
                x += (value - center(i - 1)._1) / (bins1(m)._1 - bins1(m - 1)._1) * (bins1(m)._2 + bins1(m - 1)._2) / 2
              } else {
                x += (value - bins1(m - 1)._1) / (bins1(m)._1 - bins1(m - 1)._1) * (bins1(m)._2 + bins1(m - 1)._2) / 2
              }
              flag = false
            }
          }
          m = 1
          value = center(i)._1 + ScalaNe2.range
          flag = true
          while (m < bins2.size && flag) {
            if (value >= bins2(m)._1) {
              if (i > 0 && center(i - 1)._1 > bins2(m - 1)._1) {
                //              y += (bins2(m - 1)._1 - center(i - 1)._1) / (bins2(m)._1 - bins2(m - 1)._1) * (bins2(m)._2 + bins2(m - 1)._2) / 2
                y += (bins2(m)._1 - center(i - 1)._1) / (bins2(m)._1 - bins2(m - 1)._1) * (bins2(m)._2 + bins2(m - 1)._2) / 2
              } else {
                y += (bins2(m)._2 + bins2(m - 1)._2) / 2
              }
              if (i > 0 && center(i - 1)._1 >= bins2(m - 1)._1) {
                bins2.remove(m - 1)
              } else {
                m += 1
              }
            } else {
              if (i > 0 && center(i - 1)._1 > bins2(m - 1)._1) {
                y += (value - center(i - 1)._1) / (bins2(m)._1 - bins2(m - 1)._1) * (bins2(m)._2 + bins2(m - 1)._2) / 2
              } else {
                y += (value - bins2(m - 1)._1) / (bins2(m)._1 - bins2(m - 1)._1) * (bins2(m)._2 + bins2(m - 1)._2) / 2
              }
              flag = false
            }
          }
          println("x=" + x + " \ty=" + y)
          center(i) = (center(i)._1, x * y)
          i += 1
        }
        area = center

      case 1 =>

        /**
          * 密度
          */
        var i = 0
        while (i < center.size) {
          var value = center(i)._1
          var m = 1
          var x = 0.0
          var y = 0.0
          var flag = true
          while (m < bins1.size && flag) {
            if (value >= bins1(m)._1) {
              if (i > 0 && center(i - 1)._1 > bins1(m - 1)._1) {
                x += (bins1(m)._1 - center(i - 1)._1) / (bins1(m)._1 - bins1(m - 1)._1) * (bins1(m)._2 + bins1(m - 1)._2) / 2
              } else {
                x += (bins1(m)._2 + bins1(m - 1)._2) / 2
              }
              if (i > 0 && center(i - 1)._1 >= bins1(m - 1)._1) {
                bins1.remove(m - 1)
              } else {
                m += 1
              }
            } else {
              if (i > 0 && center(i - 1)._1 > bins1(m - 1)._1) {
                x += (value - center(i - 1)._1) / (bins1(m)._1 - bins1(m - 1)._1) * (bins1(m)._2 + bins1(m - 1)._2) / 2
              } else {
                x += (value - bins1(m - 1)._1) / (bins1(m)._1 - bins1(m - 1)._1) * (bins1(m)._2 + bins1(m - 1)._2) / 2
              }
              flag = false
            }
          }
          m = 1
          value = center(i)._1 + ScalaNe2.range
          flag = true
          while (m < bins2.size && flag) {
            if (value >= bins2(m)._1) {
              if (i > 0 && center(i - 1)._1 > bins2(m - 1)._1) {
                y += (bins2(m)._1 - center(i - 1)._1) / (bins2(m)._1 - bins2(m - 1)._1) * (bins2(m)._2 + bins2(m - 1)._2) / 2
              } else {
                y += (bins2(m)._2 + bins2(m - 1)._2) / 2
              }
              if (i > 0 && center(i - 1)._1 >= bins2(m - 1)._1) {
                bins2.remove(m - 1)
              } else {
                m += 1
              }
            } else {
              if (i > 0 && center(i - 1)._1 > bins2(m - 1)._1) {
                y += (value - center(i - 1)._1) / (bins2(m)._1 - bins2(m - 1)._1) * (bins2(m)._2 + bins2(m - 1)._2) / 2
              } else {
                y += (value - bins2(m - 1)._1) / (bins2(m)._1 - bins2(m - 1)._1) * (bins2(m)._2 + bins2(m - 1)._2) / 2
              }
              flag = false
            }
          }
          var range = 0.0
          if (i == 0) {
            if (bins1(0)._1 > bins2(0)._1) {
              range = center(0)._1 - bins2(0)._1
            } else {
              range = center(0)._1 - bins1(0)._1
            }
          } else {
            range = center(i)._1 - center(i - 1)._1
          }
          // 修改后
          if (range < ScalaNe2.range) {
            range = ScalaNe2.range
          }
          val density = (x / range * ScalaNe2.range) * (y / range * ScalaNe2.range) * (range / ScalaNe2.range)
          println("x=" + x + " \ty=" + y + " \trange=" + range + " \tdensity=" + density)
          center(i) = (center(i)._1, density)
          i += 1
        }
        area = center

      case _ => System.err.println("schema option")
    }
    area
  }


  def computeArea(list1: ArrayBuffer[(Double, Double)], list2: ArrayBuffer[(Double, Double)]) = {
    val result = ArrayBuffer.empty[(Double, Double)]
    list1.foreach { s =>
      var i = 0
      while (i < list2.size && list2(i)._1 - s._1 < -range) {
        list2.remove(i)
      }
      var value = 0d
      while (i < list2.size && list2(i)._1 - s._1 < range) {
        value += (range - SamplingUtils.abs(s._1 - list2(i)._1)) / range * s._2 * list2(i)._2
        i += 1
      }
      result += ((s._1, value))
    }
    result
  }

  /**
    * 1000 => 10 1000个样本划分成10个分区
    */
  def determineBounds(list: ArrayBuffer[(Double, Double)]): Array[Double] = {
    val ordering = implicitly[Ordering[Double]]
    val numCandidates = list.size
    val sumWeights = list.map(_._2.toDouble).sum
    val step = sumWeights / ScalaNe2.partitions
    var cumWeight = 0.0
    var target = step
    val bounds = ArrayBuffer.empty[Double]
    var i = 0
    var j = 0
    var previousBound = Option.empty[Double]

    while ((i < numCandidates) && (j < ScalaNe2.partitions - 1)) {
      val (key, weight) = list(i)
      cumWeight += weight
      if (cumWeight > target) {
        // Skip duplicate values.
        if (previousBound.isEmpty || ordering.gt(key, previousBound.get)) {
          bounds += key
          target += step
          j += 1
          previousBound = Some(key)
        }
      }
      i += 1
    }
    bounds.toArray
  }
}


class DTSampling(rdd: RDD[Double]) {

  def samples(isMerge: Boolean): ArrayBuffer[(Double, Int)] = {
    // 需要合并 bins 操作时使用,每个父partition产生一个bins
    val part: Int = ScalaNe2.part
    val scale: Int = ScalaNe2.scale
    val equalHeight: Boolean = true
    val dth: DecisionTreeHistogram = new DecisionTreeHistogram(true)
    dth.allocate(part, scale, equalHeight)

    val bins = ArrayBuffer.empty[(Double, Int)]
    val (numItems, sketched) = sketch()
    if (numItems == 0L) {
      Array.empty
    } else {
      if (isMerge) {
        // 合并 bins
        sketched.foreach { s =>
          dth.merge(DecisionTreeHistogram.samplesToBins(s._2))
        }
        dth.samples().map(l => (l._1, l._2)).foreach { s =>
          bins += ((s._1, s._2))
        }
      } else {
        // 不和并 bins
        sketched.foreach { s =>
          s._2.foreach { ss =>
            bins += ((ss._1, ss._2))
          }
        }
        bins.sortBy(_._1)
      }
    }
    bins.insert(0, (bins(0)._1 * 2 - bins(1)._1, 0))
    bins += ((bins(bins.length - 1)._1 * 2 - bins(bins.length - 2)._1, 0))
    bins
    // 在bins两端添加左右两头端点
    //    val cacheBins = ArrayBuffer.empty[(Double, Int)]
    // 添加左端点
    //    cacheBins += ((bins(0)._1 * 2 - bins(1)._1, 0))
    // 将全部元素复制到新的 cacheBins 中去
    //    bins.foreach { s =>
    //      cacheBins += s
    //    }
    // 添加右端点
    //    cacheBins += ((bins(bins.length - 1)._1 * 2 - bins(bins.length - 2)._1, 0))
    //
    //    cacheBins
  }

  /**
    * 采样
    */
  def sketch() = {
    val sketched = rdd.mapPartitionsWithIndex {
      (idx, iter) =>

        val part: Int = ScalaNe2.part
        val scale: Int = ScalaNe2.scale
        val equalHeight: Boolean = true
        val dth: DecisionTreeHistogram = new DecisionTreeHistogram(true)
        dth.allocate(part, scale, equalHeight)

        var count = 0
        iter.foreach {
          item => dth.addItem(item) // 建立bins
            count += 1
        }
        val sample = dth.samples().map(l => (l._1, l._2))

        Iterator((count, sample))
    }.collect()

    val numItems = sketched.map(_._1.toLong).sum
    (numItems, sketched)
  }

}

