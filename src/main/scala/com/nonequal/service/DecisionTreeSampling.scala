package com.nonequal.service

import com.nonequal.task.ScalaNe2
import com.nonequal.util.BinarySearchUtil
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by hanmz on 16-3-28.
  *
  * 通过决策树算法获取抽样数据
  */
object DecisionTreeSampling {
  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("DecisionTreeSampling").setMaster("spark://localhost:7077")
    val sc = new SparkContext(sparkConf)
    val skew = sc.textFile("hdfs://localhost:9000/user/data/skew1", 1)
    val skews = skew.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('l', key, s))
    }
    val skew2 = sc.textFile("hdfs://localhost:9000/user/data/skew2", 1)
    val skew2s = skew2.map { s =>
      val key = s.split("\\t")(0).toDouble
      (key, ('l', key, s))
    }

    // 主表抽样
    val result = new DecisionTreeSampling(skews.map(s => s._1), 'S').samples()
    var i = 0
    println("主表：")
    result.foreach { s => i += 1
      println(i + " " + s._1 + " " + s._2)
    }

    // 副表抽样 + 连接操作
    val result1 = new DecisionTreeSampling(skew2s.map(s => s._1), 'S').samples()
    println("副表：")
    result1.foreach { s => i += 1
      println(i + " " + s._1 + " " + s._2)
    }

    //    // 输出连接结果
    //    val r = result1.joinNums
    //    println("join result:")
    //    i = 0
    //    r.foreach { s =>
    //      i += 1
    //      println(i + " " + s)
    //    }
  }

  def join(item: Double, arr: Array[Double], buffer: ArrayBuffer[Long]) = {
    val length = arr.length

    var index = BinarySearchUtil.binarySearch(arr, item)
    if (index < 0) {
      index = -index - 1
    }
    if (index > length) {
      index = length
    }
    var left = index - 1
    var right = index
    if (index == 0) {
      left = 0
      right = 1
    }
    while (left >= 0 && left < length && BinarySearchUtil.abs(arr(left), item) <= ScalaNe2.range) {
      buffer(left) = buffer(left) + 1
      left -= 1
    }
    while (right < length && right > 0 && BinarySearchUtil.abs(arr(right), item) <= ScalaNe2.range) {
      buffer(right) = buffer(right) + 1
      right += 1
    }
  }
}

class DecisionTreeSampling(rdd: RDD[Double], private val tag: Char) {
  /**
    * 对应每个元素的连接个数
    */
  var joinNums = Array.empty[Long]

  private def samples(arr: Array[Double] = Array.empty[Double]): (Array[(Double, Double, Char)], Long, Double) = {
    // 根据不同参数执行不同过程
    var sketchs = sketch()
    if (!arr.isEmpty) {
      sketchs = sketchAndGetJoinNum(arr)
    }

    val (numItems, sketched) = sketchs
    if (numItems == 0L) {
      (Array.empty, 0, 0d)
    } else {
      val candidates = ArrayBuffer.empty[(Double, Double, Char)]
      sketched.foreach { case (n, sample) =>
        for (key <- sample) {
          candidates += ((key._1, key._2.toDouble, tag))
        }
      }
      val result = determineBounds(candidates, ScalaNe2.scale + 1)
      val total = result.map(r => r._2).sum
      val avg = total / ScalaNe2.scale

      (result, numItems, avg)
    }
  }

  def samples(): Array[(Double, Double, Char)] = {
    val (numItems, sketched) = sketch()
    if (numItems == 0L) {
      Array.empty
    } else {
      val result = ArrayBuffer.empty[(Double, Double, Char)]
      sketched.foreach { case (n, sample) =>
        for (key <- sample) {
          result += ((key._1, key._2.toDouble, tag))
        }
      }
      result.toArray
    }
  }

  /**
    *
    * @param arr 主表中抽样元素
    * @return (Array[(Double, Double, Char)], Long, Double) 元素,权值,标志数组 元素总量 平均权值
    */
  def obtainSamples(arr: Array[Double] = Array.empty[Double]): (Array[(Double, Double, Char)], Long, Double) = {
    if (arr.isEmpty) {
      samples()
    } else {
      samples(arr)
    }
  }

  /**
    * 主表采样
    *
    * @return 元素总量 样本,权重
    */
  def sketch() = {
    val sketched = rdd.mapPartitionsWithIndex {
      (idx, iter) =>

        val part: Int = 8
        val scale: Int = ScalaNe2.scale
        val equalHeight: Boolean = true
        val dth: DecisionTreeHistogram = new DecisionTreeHistogram(true)
        dth.allocate(part, scale, equalHeight)

        var count = 0
        iter.foreach { item => dth.addItem(item)
          count += 1
        }
        val list = dth.samples()
        val sample = list.map(l => (l._1, l._2)).array

        Iterator((count, sample))
    }.collect()

    val numItems = sketched.map(_._1.toLong).sum
    (numItems, sketched)
  }

  /**
    * 副表采样
    * 采样的同时进行主表样本连接数计算操作,将连接数信息写入成员变量joinNums中
    *
    * @param arr 主表样本
    * @return 元素总量 样本,权重
    */
  def sketchAndGetJoinNum(arr: Array[Double]) = {
    val sketched = rdd.mapPartitionsWithIndex {
      (idx, iter) =>

        val part: Int = 10
        val scale: Int = ScalaNe2.scale
        val equalHeight: Boolean = true
        val dth: DecisionTreeHistogram = new DecisionTreeHistogram(true)
        dth.allocate(part, scale, equalHeight)

        val length = arr.length
        val buffer = ArrayBuffer.empty[Long]
        var i = 0
        while (i < length) {
          buffer += 0
          i += 1
        }

        var count = 0
        iter.foreach { item =>
          dth.addItem(item)
          count += 1
          DecisionTreeSampling.join(item, arr, buffer)
          //          DecisionTreeSampling.han(item, arr) // 可以访问类变量，不能访问实例变量
        }
        val list = dth.samples()
        val sample = list.map(l => (l._1, l._2)).array

        Iterator((count, sample, buffer.toArray))
    }.collect()

    val length = arr.length
    val buffer = ArrayBuffer.empty[Long]
    var i = 0
    while (i < length) {
      buffer += 0l
      i += 1
    }
    i = 0
    while (i < length) {
      buffer(i) = sketched.map(_._3.__leftOfArrow(i)).sum
      i += 1
    }
    joinNums = buffer.toArray

    val numItems = sketched.map(_._1.toLong).sum
    (numItems, sketched.map(s => (s._1, s._2)))
  }

  def determineBounds(candidates: ArrayBuffer[(Double, Double, Char)], partitions: Int): Array[(Double, Double, Char)] = {
    val ordering = implicitly[Ordering[Double]]
    val ordered = candidates.sortBy(_._1)
    val numCandidates = ordered.size
    val sumWeights = ordered.map(_._2.toDouble).sum
    val step = sumWeights / partitions
    var cumWeight = 0.0
    val bounds = ArrayBuffer.empty[(Double, Double, Char)]
    var i = 0
    var j = 0
    var previousBound = Option.empty[Double]
    while ((i < numCandidates) && (j < partitions - 1)) {
      val (key, weight, tag) = ordered(i)
      cumWeight += weight
      if (cumWeight >= step) {
        // Skip duplicate values.
        if (previousBound.isEmpty || ordering.gt(key, previousBound.get)) {
          bounds += ((key, cumWeight, tag))
          cumWeight = 0
          j += 1
          previousBound = Some(key)
        }
      }
      i += 1
    }
    bounds.toArray
  }
}
