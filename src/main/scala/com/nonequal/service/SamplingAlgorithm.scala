package com.nonequal.service

import com.nonequal.util.SamplingUtils
import org.apache.spark.rdd.{PartitionPruningRDD, RDD}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
import scala.util.hashing.byteswap32

/**
  * Created by hanmz on 16-3-6.
  *
  */
class SamplingAlgorithm[K: Ordering : ClassTag, V](@transient partitions: Int,
                                                   @transient rdd: RDD[_ <: Product2[K, V]],
                                                   private val tag: Char,
                                                   private var ascending: Boolean = true
                                                  ) {

  // We allow partitions = 0, which happens when sorting an empty RDD under the default settings.
  require(partitions >= 0, s"Number of partitions cannot be negative but found $partitions.")

  // An array of upper bounds for the first (partitions - 1) partitions
  private val samples: (Array[(K, Float, Char)], Long, Map[Int, Float]) = {
    if (partitions <= 1) {
      (Array.empty, 0, Map.empty)
    } else {
      // This is the sample size we need to have roughly balanced output partitions, capped at 1M.
      val sampleSize = math.min(200.0 * partitions, 1e6)
      // Assume the input partitions are roughly balanced and over-sample a little bit.
      val sampleSizePerPartition = math.ceil(3.0 * sampleSize / rdd.partitions.length).toInt
      val (numItems, sketched) = SamplingAlgorithm.sketch(rdd.map(_._1), sampleSizePerPartition)
      if (numItems == 0L) {
        (Array.empty, 0, Map.empty)
      } else {
        // If a partition contains much more than the average number of items, we re-sample from it
        // to ensure that enough items are collected from that partition.
        val fraction = math.min(sampleSize / math.max(numItems, 1L), 1.0)
        val candidates = ArrayBuffer.empty[(K, Float, Char)]
        var indexAndWeight: Map[Int, Float] = Map.empty
        val imbalancedPartitions = mutable.Set.empty[Int]
        sketched.foreach { case (idx, n, sample) =>
          if (fraction * n > sampleSizePerPartition) {
            imbalancedPartitions += idx
          } else {
            // The weight is 1 over the sampling probability.
            val weight = (n.toDouble / sample.length).toFloat
            for (key <- sample) {
              candidates += ((key, weight, tag))
            }
            indexAndWeight += (idx -> weight)
          }
        }
        if (imbalancedPartitions.nonEmpty) {
          // Re-sample imbalanced partitions with the desired sampling probability.
          val imbalanced = new PartitionPruningRDD(rdd.map(_._1), imbalancedPartitions.contains)
          val seed = byteswap32(-rdd.id - 1)
          val reSampled = imbalanced.sample(withReplacement = false, fraction, seed).collect()
          val weight = (1.0 / fraction).toFloat
          candidates ++= reSampled.map(x => (x, weight, tag))
          indexAndWeight += (-1 -> weight)
        }
        (candidates.toArray, numItems, indexAndWeight)
      }
    }
  }

  def obtainSamples(): (Array[(K, Float, Char)], Long, Map[Int, Float]) = {
    samples
  }

}

private object SamplingAlgorithm {

  def sketch[K: ClassTag](rdd: RDD[K], sampleSizePerPartition: Int): (Long, Array[(Int, Int, Array[K])]) = {
    val shift = rdd.id
    // val classTagK = classTag[K] // to avoid serializing the entire partitioner object
    val sketched = rdd.mapPartitionsWithIndex { (idx, iter) =>
      val seed = byteswap32(idx ^ (shift << 16))
      val (sample, n) = SamplingUtils.reservoirSampleAndCount(
        iter, sampleSizePerPartition, seed)
      Iterator((idx, n, sample))
    }.collect()
    val numItems = sketched.map(_._2.toLong).sum
    (numItems, sketched)
  }

}
